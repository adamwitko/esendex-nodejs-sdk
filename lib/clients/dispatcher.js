var Dispatcher = function(client, buildUri, messagebuilder){
  this.client = client;
  this.buildUri = buildUri;
  this.buildMessage = messagebuilder;
};

Dispatcher.prototype.send = function(credentials, message, next) {
    var options = {
      credentials: credentials,
      data: this.buildMessage(message),
      uri: this.buildUri({version: 'v1.0', resource: 'messagedispatcher'})
    };

    this.client.post(options, function(error, data) {
      next(error, data)
    });
};

module.exports = Dispatcher;