var XMLMessageBuilder = function(xmlBuilder) {
  this.xmlBuilder = xmlBuilder;
};

XMLMessageBuilder.prototype.create = function(message, next) {
  next(this.xmlBuilder.create(message).end({pretty: true}));
};

module.exports = XMLMessageBuilder;