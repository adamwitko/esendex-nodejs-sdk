var uriBuilder = function (stringFormatter) {
  this.stringFormatter = stringFormatter || require('url').format;
}

uriBuilder.prototype.create = function(options) {
  return this.stringFormatter({
    protocol: 'https',
    host: 'api.esendex.com',
    pathname: options.version + '/' + options.resource
  });
};

module.exports = uriBuilder;