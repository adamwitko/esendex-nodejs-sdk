module.exports = function(request, xmlParser) {
  
  var execute = function(options, next) {
    request(options, function(error, response, body){
      xmlParser(body, {trim: true, mergeAttrs: true}, function(error, data) {
        next(error, data);
      });      
    })
  };

  var post = function(options, next) {
    var requestOptions = {
      url: options.uri,
      body: options.data,
      method: 'POST',
      auth: {
        user: options.credentials.username,
        pass: options.credentials.password
      }
    };

    execute(requestOptions, function(error, data){
      next(error, data);
    });
  };

  var get = function(options, next) {
    var requestOptions = {
      url: options.uri,
      method: 'GET',
      auth: {
        user: options.credentials.username,
        pass: options.credentials.password
      }
    };

    execute(requestOptions, function(error, data){
      next(error, data);
    });
  };

  return {
    post: post,
    get: get
  };
};