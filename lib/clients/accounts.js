module.exports = function (client, uriBuilder) {
  
  var getAll = function(credentials, next) {
    var clientOptions = {
      uri: uriBuilder.create({version: 'v1.0', resource: 'accounts'}),
      credentials: credentials
    };

    client.get(clientOptions, function(error, data){
      next(error, data);
    });
  };

  return {
    getAll: getAll
  };
};