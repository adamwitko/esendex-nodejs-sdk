module.exports = function(username, password) {
  if (username === undefined)
    throw new Error('Username is required');

  if (password === undefined)
    throw new Error('Password is required');

    return {
      username: username,
      password: password
    };
};