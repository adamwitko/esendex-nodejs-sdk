var should = require('should');
var rek = require('rekuire');
var util = require('util');

var basicAuthentication = rek('basic-authentication.js');

describe('basic authentication', function() {
  describe('when encoding username and password', function() {

    var basicAuth;

    before(function(){
      basicAuth = basicAuthentication('user@user.com','test123');
    });

    it('then the username is set', function() {
      basicAuth.should.have.property('username');
    });

    it('then the password is set', function() {
      basicAuth.should.have.property('password');
    });
  });

  describe('when encoding without username', function(){

    var thrownException;

    before(function(){
      try
      {
        basicAuthentication(undefined, 'test123');
      }
      catch (e)
      {
        thrownException = e;
      }
    });

    it('then an exception is thrown', function(){
      util.isError(thrownException);
    });

    it('then the exception contains the username is required message', function() {
      thrownException.should.have.property('message', 'Username is required');
    });
  });

  describe('when encoding without password', function(){

    var thrownException;

    before(function(){
      try
      {
        basicAuthentication('user@user.com', undefined);
      }
      catch (e)
      {
        thrownException = e;
      }
    });

    it('then an exception is thrown', function(){
      util.isError(thrownException);
    });

    it('then the exception contains the password is required message', function() {
      thrownException.should.have.property('message', 'Password is required');
    });
  });
});