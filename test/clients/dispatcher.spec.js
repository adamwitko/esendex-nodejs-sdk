var should = require('should');
var sinon = require('sinon');
var rek = require('rekuire');

var dispatcher = rek('dispatcher.js');

describe('message dispatcher', function() {
  describe('when sending SMS', function () {

    var actualMessageSentResult;
    var expectedMessageSentResult;
    var mockHttpClient;

    before(function(done) {
      expectedMessageSentResult = {
        'messageheaders' : {
          'batchid': 'F8BF9867-FF81-49E4-ACC5-774DE793B776',
          'xmlns': 'http://api.esendex.com/ns/',
          'messageheader' : [{
            'uri': 'https://api.esendex.com/v1.0/MessageHeaders/1183C73D-2E62-4F60-B610-30F160BDFBD5',
            'id': '1183C73D-2E62-4F60-B610-30F160BDFBD5'
          }]
        }
      };

      var xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                "<messageheaders batchid=\"F8BF9867-FF81-49E4-ACC5-774DE793B776\" xmlns=\"http://api.esendex.com/ns/\">"+
                  "<messageheader uri=\"https://api.esendex.com/v1.0/MessageHeaders/1183C73D-2E62-4F60-B610-30F160BDFBD5\""+
                      "id=\"1183C73D-2E62-4F60-B610-30F160BDFBD5\" />"+
                "</messageheaders>";

      var basicAuthCredentials = {
        username: "user@user.com",
        password: "password1"
      };

      var message = '<?xml version="1.0" encoding="utf-8?><messages><accountreference>EX0001010</accountreference>' + 
                    '<message><to>440393939</to><body>test</body></message>';

      var messagebuilder = sinon.stub().returns(message);
      
      var clientPostData = {
        credentials: basicAuthCredentials,
        data: message,
        uri: "some uri"
      };

      var buildUri = sinon.stub();
      buildUri
        .withArgs({version: "v1.0", resource:"messagedispatcher"})
        .returns(clientPostData.uri);

      var http = { post: function(options, next){}};

      mockHttpClient = sinon.mock(http);
      mockHttpClient
        .expects('post')
        .once()
        .withArgs(clientPostData, sinon.match.func)
        .callsArgWith(1, undefined, expectedMessageSentResult);

      var messageDispatcher = new dispatcher(http, buildUri, messagebuilder);

      messageDispatcher.send(basicAuthCredentials, message, function(err, data) {
        actualMessageSentResult = data;
        done();
      });
    });

    it('should send the message', function() {
      mockHttpClient.verify();
    });

    it('should return a message sent result', function() {
      actualMessageSentResult.should.eql(expectedMessageSentResult);
    });
  });
}); 