var should = require('should');
var sinon = require('sinon');
var xml2js = require('xml2js');
var rek = require('rekuire');

var account = rek('accounts.js');

describe('accounts', function(){
  describe('when getting accounts',function() {

    var actual;    
    var expectedAccountGetResult;
    var httpClientMock;
    var uriBuilderMock;

    before(function(done) {
      
      expectedAccountGetResult = {
        "accounts": {
          "xmlns": "http://api.esendex.com/ns/",
          "account": {
            "id": "A00F0218-510D-423D-A74E-5A65342FE070",
            "uri": "http://api.esendex.com/v1.0/accounts/A00F0218-510D-423D-A74E-5A65342FE070",
            "reference": "EX0000000",
            "label": "EX0000000",
            "address": "447700900654",
            "type": "Professional",
            "messagesremaining": "2000",
            "expireson": "2999-02-25T00:00:00",
            "role": "PowerUser",
            "settings": { 
              "uri": "http://api.esendex.com/v1.0/accounts/A00F0218-510D-423D-A74E-5A65342FE070/settings" 
            }
          }
        }
      };

      var clientOptions = {
        credentials: {
          username: "user@user.com",
          password: "password"
        },
        uri: "some uri"
      };

      var uriBuilder = {
        create: function(options){ }
      };

      uriBuilderMock = sinon.mock(uriBuilder);
      uriBuilderMock
        .expects("create")
        .once()
        .withArgs({version: "v1.0", resource: "accounts"})
        .returns(clientOptions.uri);

      var httpClient = { 
        get: function(){}
      };

      httpClientMock = sinon.mock(httpClient);
      httpClientMock
        .expects('get')
        .once()
        .withArgs(clientOptions, sinon.match.func)
        .callsArgWith(1, undefined, expectedAccountGetResult);

      account(httpClient, uriBuilder).getAll(clientOptions.credentials, function(error, data){
        actual = data;
        done();
      });
    });

    it('should return the user accounts', function() {
      expectedAccountGetResult.should.eql(actual);
    });

    it('creates the accounts uri ', function() {
      uriBuilderMock.verify();
    });

    it('executes a GET request', function() {
      httpClientMock.verify();
    });
  });
});