var should = require('should');
var sinon = require('sinon');
var rek = require('rekuire');

var xmlMessageBuilder = rek('xml-message-builder.js');

describe('message builder', function() {
  describe('when building an individual message', function () {
    
    var actual;
    var expected;

    beforeEach(function(done){

      var message = { something: 'a' };

      expected = 'expected message';

      root = {
          end: sinon.stub().returns(expected)
      };

      var xmlbuilder = { create: sinon.stub() };
      xmlbuilder.create.returns(root);

      var builder = new xmlMessageBuilder(xmlbuilder);

      builder.create(message, function(data) {
        actual = data;
        done();
      })
    });

    it('should return the expected message string', function() {
      actual.should.eql(expected);
    });
  })
});