var should = require('should');
var sinon = require('sinon');

var rek = require('rekuire');
var httpClient = rek('http-client.js');

describe('http client', function() {
  describe('when POST', function() {

    var mockRequest;
    var responseBody;
    var expectedResult;

    before(function(done){

      expectedResult = "hey";

      var options = {
        uri: "https://api.esendex.com/post",
        credentials: {
          username: "user@user.com",
          password: "lojai"
        },
        data: "<somexml>hhehe</somexml>"
      };

      var requestResponseBody = "some response data";

      mockRequest = sinon.mock();
      mockRequest
        .once()
        .withArgs({
          url: options.uri,
          body: options.data,
          method: "POST",
          auth: {
            user: options.credentials.username,
            pass: options.credentials.password
          }
        }, sinon.match.func)
        .callsArgWith(1, undefined, {}, requestResponseBody);

      var xmlParser = sinon.stub();
      xmlParser
        .withArgs(requestResponseBody, sinon.match.object, sinon.match.func)
        .callsArgWith(2, undefined, expectedResult);

      httpClient(mockRequest, xmlParser).post(options, function(err, data){
        responseBody = data;
        done();
      });
    });

    it('then the data is sent', function() {
      mockRequest.verify();
    });

    it('then the post response body is returned by the callback', function() {
      responseBody.should.eql(expectedResult);
   });
  });

  describe('when GET', function() {

    var mockRequest;
    var actual;
    var expected;

    before(function(done){
      expected = "hey";

      var options = {
        uri: "https://api.esendex.com/get",
        credentials: {
          username: "get@user.com",
          password: "whut"
        }
      };

      var requestResponseBody = "some response data";

      mockRequest = sinon.mock();
      mockRequest
        .once()
        .withArgs({
          url: options.uri,
          method: "GET",
          auth: {
            user: options.credentials.username,
            pass: options.credentials.password
          }
        }, sinon.match.func)
        .callsArgWith(1, undefined, {}, requestResponseBody);

      var xmlParser = sinon.stub();
      xmlParser
        .withArgs(requestResponseBody, sinon.match.object, sinon.match.func)
        .callsArgWith(2, undefined, expected);

      httpClient(mockRequest, xmlParser).get(options, function(err, data) {
        actual = data;
        done();
      });
    });

    it('then the data is sent', function() {
      mockRequest.verify();
    });

    it('then the post response body is returned by the callback', function() {
      actual.should.eql(expected);
   });
  });
});