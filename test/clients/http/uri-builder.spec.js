var should = require('should');
var sinon = require('sinon');

var rek = require('rekuire');
var uriBuilder = rek('uri-builder.js');

describe('resource uri builder', function() {
  describe('when creating uri', function() {

    var result;

    before(function(){
      var options = {
        resource: "messagedispatcher",
        version: "v1.0",
      };

      var stringFormatter = sinon.stub();

      stringFormatter
        .returns("https://api.esendex.com/v1.0/messagedispatcher");

      result = new uriBuilder(stringFormatter).create(options);
    });

    it('then the generated uri is correct', function() {
      result.should.eql("https://api.esendex.com/v1.0/messagedispatcher");
    });
  });
});