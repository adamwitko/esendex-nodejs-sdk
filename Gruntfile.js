module.exports = function(grunt){

	grunt.loadNpmTasks('grunt-mocha-test');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.initConfig({
		pck: grunt.file.readJSON('package.json'),
		mochaTest: {
			test: {
				options: {
					reporter: 'spec'
				},
				src: ['test/**/*.js']
			}
		},
		watch: {
			scripts: {
				files: '**/*.js',
				tasks: 'mochaTest'
			}
		}
	});

	grunt.registerTask('default', ['mochaTest', 'watch']);
};